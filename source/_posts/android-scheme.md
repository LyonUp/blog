---
layout: post
title:  Android通过Scheme外部唤起
date:   2017-3-21 21:48:12 +0800
thumbnail: https://i.loli.net/2017/08/23/599d802172916.jpg
categories: 技术
tags: Android
---
使用Scheme 从浏览器或其他应用中唤起本应用。





# 外部唤起Android应用

只是简单记录一下~



### 0.Scheme组成 

``` java
          <data android:scheme="string"
                android:host="string"
                android:port="string"
                android:path="string"
                android:pathPattern="string"
                android:pathPrefix="string"
                android:mimeType="string" />
```







### 1. 注册响应scheme的Activity

``` groovy
            <intent-filter>
                <action android:name="android.intent.action.VIEW" />
                <category android:name="android.intent.category.DEFAULT" />
                <category android:name="android.intent.category.BROWSABLE"/>
                <data android:scheme="something" />
                <data android:host="project.example.com" />
            </intent-filter>
```







### 2.跳转入口

``` java
			// 只含了scheme和host
			"something://project.example.com"
```









### 3.获取参数

``` java 
        Intent intent =getIntent();
        sUri = intent.getData();
        uri.getScheme();
        uri.getHost();
```


